#!/bin/bash

export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i ansible-hosts prepare-volumes.yml
ansible-playbook -i ansible-hosts prepare-registry.yml

rm -rf install/build/.conf
