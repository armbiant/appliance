#!/bin/bash

set -xe

VERSION=v0.8.1

echo Launching Merge Appliance version $VERSION

if [[ ! -f vm-$VERSION.qcow2 ]]; then
    curl -OL https://storage.googleapis.com/appliance.mergetb.dev/vm-$VERSION.qcow2
fi
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/vm-$VERSION.xml
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/net-$VERSION.xml
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/key-$VERSION
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/key-$VERSION.pub
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/kubeconfig-$VERSION
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/kubeadmin-password-$VERSION
curl -OL https://storage.googleapis.com/appliance.mergetb.dev/portal-genconf-$VERSION.yml

sudo virsh destroy sno-test || true

sudo virsh net-define net-$VERSION.xml
sudo virsh define vm-$VERSION.xml
sudo cp vm-$VERSION.qcow2 /var/lib/libvirt/images/sno-test.qcow2

sudo virsh net-start --network test-net || true
sudo virsh start sno-test

grep -q '^192.168.126.10' /etc/hosts || cat >> /etc/hosts <<EOF
192.168.126.10 api.test-cluster.redhat.com
192.168.126.10 console-openshift-console.apps.test-cluster.redhat.com
192.168.126.10 oauth-openshift.apps.test-cluster.redhat.com
192.168.126.10 default-route-openshift-image-registry.apps.test-cluster.redhat.com
192.168.126.10 mergetb.example.net
192.168.126.10 api.mergetb.example.net
192.168.126.10 git.mergetb.example.net
192.168.126.10 grpc.mergetb.example.net
192.168.126.10 auth.mergetb.example.net
192.168.126.10 jump.mergetb.example.io
EOF
