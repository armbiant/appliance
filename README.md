# Merge Portal Appliance

This repository produces the following things.

- A QCOW2 virtual mahcine with
  - OpenShift pre-installed
  - The Merge portal software pre-installed
- A set of libvirt xml specs for spinning up
  - The virtual machine in the QCOW2 image
  - The required networking around the virtual machine
- A script for setting up local name resolution

Further guidance
- [Building](BUILDING.md)
- [Using](USING.md)
