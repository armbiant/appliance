#!/bin/bash

source common.sh

unset KUBECONFIG

pushd wg

rm wireguard.ko
if [[ ! -f wireguard.rpm ]]
then
    wget -O wireguard.rpm https://mirror.rackspace.com/elrepo/elrepo/el8/x86_64/RPMS/kmod-wireguard-1.0.20210606-1.el8_4.elrepo.x86_64.rpm 
fi

rpm2cpio wireguard.rpm | cpio -ivd "*.ko"
mv $(find . -name "*.ko") wireguard.ko
find . -empty -type d -delete

scp -o StrictHostKeyChecking=no -i $CORE_SSHKEY wireguard.ko core@192.168.126.10:~/
ssh -o StrictHostKeyChecking=no -x -i $CORE_SSHKEY core@192.168.126.10 "
sudo mkdir -p /opt/wireguard && 
sudo cp wireguard.ko /opt/wireguard/ && 
sudo rmmod /opt/wireguard/wireguard.ko | true && 
sudo insmod /opt/wireguard/wireguard.ko && 
echo Wireguard module installed"

popd  # wgbuilder
