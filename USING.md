# Using

These instructions assume the current Fedora, RHEL or CentOS release with the 
`@virtualization` software group installed.

## tl;dr

```
curl -L https://gitlab.com/mergetb/portal/appliance/-/raw/master/launch-appliance.sh | bash
```

## Approving CSRs

When using the portal appliance certificates may become out of date, and
certificate signing requests may need to be approved.

### Symptoms

You cannot access the Merge API

```
[ry@ryzen2 portal]$ mrg logout
FATA logout: rpc error: code = Unavailable desc = connection error: desc = "transport: Error while dialing dial tcp 192.168.130.11:443: connect: no route to host"
```

You cannot login with `oc` using developer credentials

```
[ry@ryzen2 v0.7]$ oc login     --insecure-skip-tls-verify=true     -u developer     -p developer     https://api.test-cluster.redhat.com:6443
The connection to the server oauth-openshift.apps.test-cluster.redhat.com was refused - did you specify the right host or port?
```

### Identifying

```
oc get csr
```

Returns a list of **pending** certificate signing requests.

### Resolving

```
oc get csr -o name | xargs oc adm certificate approve
```

You may need to do this several times, as approving some CSRs creates more.

## Details

The appliance is delivered as the artifacts available at the following links
- [A VM Image](https://storage.googleapis.com/appliance.mergetb.dev/vm-v0.7.0.qcow2)
- [A Libvirt Domain Spec](https://storage.googleapis.com/appliance.mergetb.dev/vm-v0.7.0.xml)
- [A Libvirt Network Spec](https://storage.googleapis.com/appliance.mergetb.dev/net-v0.7.0.xml)
- [An SSH Private Key for VM access](https://storage.googleapis.com/appliance.mergetb.dev/key-v0.7.0)
- [An SSH Public Key](https://storage.googleapis.com/appliance.mergetb.dev/key-v0.7.0.pub)
- [A Kubeconfig](https://storage.googleapis.com/appliance.mergetb.dev/kubeconfig-v0.7.0)
- [A Kubeadmin Password](https://storage.googleapis.com/appliance.mergetb.dev/kubeadmin-password-v0.7.0)
- [A Portal Runtime Configuration](https://storage.googleapis.com/appliance.mergetb.dev/portal-genconf-v0.7.0.yml)

Add the network and vm spec to libvirt with `virsh` and place the vm image at 
`/var/lib/libvirt/images/sno-test.qcow2`. 

```shell
sudo virsh net-define net-v0.7.0.xml
sudo virsh define vm-v0.7.0.xml
sudo cp vm-v0.7.0.qcow2 /var/lib/libvirt/images/sno-test.qcow2
```

You should then be able to run the appliance using the libvirt frontend of your choosing.

```shell
sudo virsh net-start --network test-net
sudo virsh start sno-test
```

To access the VM, first set up your local host resolver settings with this
script
- https://gitlab.com/mergetb/portal/appliance/-/blob/master/end-user-scripts/hosts.sh

Once the VM has started, you can access OpenShift by using the [OpenShift
client](https://gitlab.com/mergetb/portal/appliance/-/raw/master/oc), and the
`kubeconfig` linked above. The console is accessible at
https://console-openshift-console.apps.test-cluster.redhat.com
with the kubeadmin password linked above. The VM hostin the portal appliance is
direclty accessible via SSH using the key material above.

Further tips and details on accessing the portal are avilable her
- https://gitlab.com/mergetb/portal/appliance/-/blob/master/BUILDING.md#tips

If the NFS service is not running the `mergefs` container will fail to create. Check it via

```shell
oc -n merge get pods | grep mergefs
````

If it is stuck in `Container Creating` you need to restart the NFS server on the appliance. You can restart it via

```shell
ssh -i key-v0.8.1 core@192.168.126.10 systemctl start nfs-server.service
```

This will only need to be done on appliance start. 

## Merge Portal Access

Grab the latest version of the
[mrg](https://gitlab.com/mergetb/portal/cli/-/jobs/artifacts/v1-staging/raw/mrg?job=build) CLI tool.

Point `mrg` at your appliance
```
mrg config set server grpc.mergetb.example.net
```

Login using the credentials from the configuration linked above
```
pw=`cat portal-genconf* | grep opspw | awk '{print $2}'`
mrg login ops $pw --nokeys
```

Test that the portal is sane.

```
mrg list ids
```
```
USERNAME    EMAIL                      ADMIN
ops         ops@mergetb.example.net    true
```

## Experimentation Quick Start

```
mrg logout
mrg register <username> <email> <password>
mrg login ops $pw --nokeys
mrg init <username>
mrg activate <username>
mrg logout
mrg login <username> <password>
# do experimenter things
```

Useful links
- https://next.mergetb.org/docs/portal/operation/#creating-identities
- https://next.mergetb.org/docs/portal/operation/#creating-users
- https://next.mergetb.org/docs/portal/operation/#activating-users

## Commissioning a testbed

To do any sort of realization or materialization, you'll need a testbed model
commissioned to your portal. In Merge the operators of facilities are distinct
from the operators of portals, so you'll need to create a distinct user that is the
'owner' of the facility. Once you have created this user and logged in as them:

```
curl -OL https://gitlab.com/mergetb/devops/vte/phobos/-/raw/master/model/cmd/phobos.xir
mrg new fac phobos phobos.example.com phobos.xir
```

Useful links
- https://next.mergetb.org/docs/facility/operate/#commissioning

**NOTE: there is currently a bug in the policy that is installed in the
appliance. You need to update the policy to
[this specification](https://gitlab.com/mergetb/portal/services/-/blob/v1-staging/pkg/policy/policy.yml)
by following 
[these directions](https://next.mergetb.org/docs/portal/operation/#updating).**


## Flushing DNS cache

If you are using your workstation's `/etc/hosts` file for quick and easy name
resolution of things (like Phobos) from within the appliance, if you need to
make a change to `/etc/hosts` to an existing entry - that will not get picked up
unless you do a

```
sudo killall -HUP dnsmasq
```

