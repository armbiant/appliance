#!/bin/bash

source common.sh

waitForCluster() {

    while true; do

        (./oc get csr -o name | xargs ./oc adm certificate approve) > /dev/null 2>&1
        out=`./oc get clusterversion`
        if [[ $? -ne 0 ]]; then
            echo "cluster not ready yet"
            sleep 5;
            continue
        fi

        if [[ "$out" == *"Cluster version is 4"* ]]; then
            break
        fi
        echo "cluster not ready yet"
        sleep 5;

    done

    echo "Cluster is ready"


}

waitForCluster
