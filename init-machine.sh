#!/bin/bash

source common.sh

if [[ -z $PULL_SECRET ]]; then
    echo "Must specify pull secret"
    exit 1
fi

export RELEASE_IMAGE=quay.io/openshift-release-dev/ocp-release:4.8.0-rc.2-x86_64
export CLUSTER_SVC_NETWORK="172.60.0.0/16"
export DISK_GB=200
export CPU_CORE=16
export RAM_MB=32768

# build launch vm and kick off OpenShift install
echo starting virtual machine and openshift install

./end-user-scripts/hosts.sh
pushd bootstrap-in-place-poc
rm -rf sno-workdir ssh-key
touch .dns_changes_confirmed
make start-iso
popd
ssh-keygen -R "192.168.126.10"

# wait for install completion (30+ mins)
echo "Waiting for cluster to become ready"

time ./wait.sh
