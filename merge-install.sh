#!/bin/bash

set -e

source common.sh

# Ensure all of the OpenShift services are running
echo "Waiting for OpenShift to come up..."
./wait.sh

echo "Making sure we can log in"
unset KUBECONFIG
oc login \
    --insecure-skip-tls-verify=true \
    -u developer \
    -p developer \
    https://api.test-cluster.redhat.com:6443
oc logout

source common.sh

config=`pwd`/portal-install.yml

pushd install/build

echo Running Preflight
../preflight.sh

echo Running install
unset KUBECONFIG
oc login \
    --insecure-skip-tls-verify=true \
    -u developer \
    -p developer \
    https://api.test-cluster.redhat.com:6443
pw=`oc whoami -t`
cp ../installer .
cp $config .

sed -i "s/password: sha256~.*/password: $pw/g" portal-install.yml
./installer --config portal-install.yml

popd

# pushd pub

# ./all-restart.sh

# popd
