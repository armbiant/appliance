#!/bin/bash

## THIS SCRIPT REQUIRES THAT YOU HAVE THE GOOGLE CLOUD gsutil INSTALLED AND
## ACCESS CREDENTIALS FOR THE MERGETB GOOGLE CLOUD TENANT
##
## https://cloud.google.com/storage/docs/gsutil_install#rpm

export VERSION=v0.8.1

gsutil cp pub/* gs://appliance.mergetb.dev
