#!/bin/bash

export MERGE_BASE_URI=${BASE_URI:-""}
export XDC_BASE_URI=${BASE_URI:-""}
export MERGE_BASE_URI=${MERGE_BASE_URI:-mergetb.example.net}
export XDC_BASE_URI=${XDC_BASE_URI:-mergetb.example.net}
export PORTAL_CONFIG=${1:-$PORTAL_CONFIG}
export PORTAL_CONFIG=${PORTAL_CONFIG:-`pwd`/vanilla-k8s/portal-install.yml}
export PORTAL_CONFIG=`readlink -f $PORTAL_CONFIG`

export CLUSTER_NETWORK="172.128.0.0/14"
export CLUSTER_SVC_NETWORK="10.244.0.0/16"
export DISK_GB=200
export CPU_CORE=16
export RAM_MB=32768

export SSH_KEY=./vanilla-k8s/workdir/key
export HOST_IP=192.168.126.10
export SSH_HOST=core@$HOST_IP
export SSH_OPTIONS="-o IdentitiesOnly=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
export IMAGE=/var/lib/libvirt/images/vanilla-k8s-sno.qcow2

export CRIO_VERSION=1.24
export KUBE_VERSION=1.24.8
export INGRESS_NGINX_VERSION=1.5.1

export REGISTRY=${MERGE_BASE_URI}:5000
export KUBECONFIG=`pwd`/vanilla-k8s/workdir/kubeconfig

export INTERNAL_REGISTRY=false

if cat $PORTAL_CONFIG | grep -i "internal:" | grep -i "registry.internal" ; then
    export INTERNAL_REGISTRY=true
fi

echo
echo base uri: $MERGE_BASE_URI
echo xdc uri: $XDC_BASE_URI
echo portal config: $PORTAL_CONFIG
echo internal registry: $INTERNAL_REGISTRY

sudo ./end-user-scripts/hosts.sh $HOST_IP $MERGE_BASE_URI $XDC_BASE_URI

pushd vanilla-k8s
    sudo rm -rf $IMAGE
    sudo rm -rf workdir
    sudo -E make start
    sudo chown -R $(id -u):$(id -g) .
popd

until ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST -- echo node ready ; do
    echo waiting for node to initialize...
    sleep 5
done

# use shell scripts because ansible wasn't really working on coreos
# install packages and stuff

set -e
cat vanilla-k8s/1-init.sh \
    | sed 's#${CRIO_VERSION}#'"$CRIO_VERSION#g" \
    | sed 's#${KUBE_VERSION}#'"$KUBE_VERSION#g" \
    | eval ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo -s'
set +e

ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo systemctl reboot &'

# wait for reboot
sleep 5

until ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST -- echo node ready ; do
    echo waiting for node to reboot...
    sleep 5
done

# install kubernetes and configure it slightly
set -e
cat vanilla-k8s/2-init.sh \
    | sed 's#${CLUSTER_SVC_NETWORK}#'"$CLUSTER_SVC_NETWORK#g" \
    | sed 's#${CLUSTER_NETWORK}#'"$CLUSTER_NETWORK#g" \
    | sed 's#${CRIO_VERSION}#'"$CRIO_VERSION#g" \
    | sed 's#${KUBE_VERSION}#'"$KUBE_VERSION#g" \
    | sed 's#${INGRESS_NGINX_VERSION}#'"$INGRESS_NGINX_VERSION#g" \
    | eval ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo -s'
set +e

# copy the kubeconfig
scp $SSH_OPTIONS -i $SSH_KEY $SSH_HOST:~/.kube/config ./vanilla-k8s/workdir/kubeconfig

# apply the persistent volumtes
for f in "etcd-pv.yml" "git-pv.yml" "kratos-pv.yml" "mergefs-pv.yml" "minio-pv.yml" "step-pv.yml" "xdc-mergefs-pv.yml" "xdc-step-pv.yml"
do
    kubectl apply -f vanilla-k8s/specs/$f
done

# install merge

pushd install
    go build

    echo Running Preflight
    if [ $INTERNAL_REGISTRY = true ]; then
        sudo ./3p-containers.sh
    fi

    mkdir -p build
    cd build/

    rm -rf .conf

    echo Running Installer
    cp ../installer .
    cp $PORTAL_CONFIG portal-install.yml
    sed -i 's/mergetb.example.net/'"$MERGE_BASE_URI/" portal-install.yml
    sed -i 's/mergetb.example.io/'"$XDC_BASE_URI/" portal-install.yml
    ./installer --config portal-install.yml
popd

# install launch
./vanilla-k8s/install-launch.sh

# wait for merge install to complete
until kubectl -n merge describe job/ops-init > /dev/null
do
    echo waiting for job/ops-init to be created...
    sleep 5
done

until kubectl -n merge wait --for=condition=complete job/ops-init --timeout=10s
do
    kubectl get pods --all-namespaces=true | grep -v Running | grep -v Completed | grep -v 'ops-init.*Error'
    echo
done

ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo podman image prune -a -f'

kubectl get pods -n merge | grep ops-init | awk '{print $1}' | xargs -- kubectl delete pod -n merge

kubectl get pods --all-namespaces=true

sudo virsh autostart sno-test || true
sudo virsh net-autostart test-net || true

echo Portal install complete!
echo
echo You may now optionally run the following command to set your kubeconfig:
echo "ln -s `pwd`/vanilla-k8s/workdir/kubeconfig ~/.kube/config"
