#!/bin/bash

export REGISTRY=mergetb.example.net:5000
export REGISTRY_PATH=merge
export XDC_PATH=xdc
export TAG=latest
export DOCKER_ARGS=--no-cache
export DOCKER=podman
export DOCKER_PUSH_ARGS=--tls-verify=false

set -ex

cd services

#sudo -E ymk -j 1 tools.yml
#sudo -E ymk clean.yml
ymk build.yml
ymk save-containers.yml
ymk push-containers.yml || true

cd ../install

mkdir -p build/containers/{merge,xdc}
cp ../services/build/*.tar build/containers/merge/

mv build/containers/merge/{ssh-jump,xdc-base,wgd}.tar build/containers/xdc/

# build installer
go build
