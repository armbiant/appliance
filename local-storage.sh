#!/bin/bash

export REGISTRY=default-route-openshift-image-registry.apps.test-cluster.redhat.com
export REGISTRY_PATH=merge
export TAG=latest
export DOCKER_ARGS=--no-cache
export DOCKER=podman
export DOCKER_PUSH_ARGS=--tls-verify=false
export INTERNAL=image-registry.openshift-image-registry.svc:5000

unset KUBECONFIG
oc login \
    --insecure-skip-tls-verify=true \
    -u developer \
    -p developer \
    https://api.test-cluster.redhat.com:6443

$DOCKER login -u $(oc whoami) -p $(oc whoami -t) $DOCKER_PUSH_ARGS $REGISTRY

$DOCKER pull quay.io/coreos/etcd:v3.4.14
$DOCKER tag etcd:v3.4.14 $REGISTRY/$REGISTRY_PATH/etcd:v3.4.14
$DOCKER push $DOCKER_PUSH_ARGS $REGISTRY/$REGISTRY_PATH/etcd:v3.4.14

$DOCKER pull docker.io/minio/minio:RELEASE.2021-01-16T02-19-44Z
$DOCKER tag minio:RELEASE.2021-01-16T02-19-44Z $REGISTRY/$REGISTRY_PATH/minio:RELEASE.2021-01-16T02-19-44Z
$DOCKER push $DOCKER_PUSH_ARGS $REGISTRY/$REGISTRY_PATH/minio:RELEASE.2021-01-16T02-19-44Z

source common.sh

oc -n merge patch deploy etcd -p "$(oc -n merge get deploy -o yaml etcd | sed "s,image: quay.io/coreos/etcd:.*,image: $INTERNAL/$REGISTRY_PATH/etcd:v3.4.14,gI")"
oc -n merge patch deploy minio -p "$(oc -n merge get deploy -o yaml minio | sed "s,image: docker.io/minio/minio:.*,image: $INTERNAL/$REGISTRY_PATH/minio:RELEASE.2021-01-16T02-19-44Z,gI")"
