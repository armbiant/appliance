#!/bin/bash

## THIS SCRIPT REQUIRES THAT YOU HAVE THE GOOGLE CLOUD gsutil INSTALLED AND
## ACCESS CREDENTIALS FOR THE MERGETB GOOGLE CLOUD TENANT
##
## https://cloud.google.com/storage/docs/gsutil_install#rpm

export VERSION=dev

set -ex

mkdir -p pub

sudo virsh dumpxml sno-test | sed '/^.*<qemu:arg.*\s*/d' > pub/vm-$VERSION.xml
sudo virsh net-dumpxml --network test-net > pub/net-$VERSION.xml
sudo cp workdir/key pub/key-$VERSION
sudo cp workdir/key.pub pub/key-$VERSION.pub
sudo cp workdir/kubeconfig pub/kubeconfig-$VERSION
if [[ ! -f pub/vm-$VERSION.qcow2 ]]; then
   sudo qemu-img convert -c -O qcow2 /var/lib/libvirt/images/sno-test.qcow2 pub/vm-$VERSION.qcow2
fi
sudo cp ../install/build/.conf/generated.yml pub/portal-genconf-$VERSION.yml

sudo chown $USER:$USER -R .
