#!/bin/bash

# there are more efficient ways of updating the appliance, but this is the easiest way
export PORTAL_CONFIG=${1:-$PORTAL_CONFIG}

# if run in vanilla-k8s, move out 1 directory
path=`pwd`
if echo `basename $path` | grep "vanilla-k8s" ; then
    export PORTAL_CONFIG=${PORTAL_CONFIG:-`pwd`/portal-install.yml}
    export PORTAL_CONFIG=`readlink -f "$PORTAL_CONFIG"`

    cd ..

else
    export PORTAL_CONFIG=${PORTAL_CONFIG:-`pwd`/vanilla-k8s/portal-install.yml}
    export PORTAL_CONFIG=`readlink -f "$PORTAL_CONFIG"`

fi

export SSH_KEY=./vanilla-k8s/workdir/key
export HOST_IP=192.168.126.10
export SSH_HOST=core@$HOST_IP
export SSH_OPTIONS="-o IdentitiesOnly=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
export MERGE_BASE_URI=${BASE_URI:-""}
export XDC_BASE_URI=${BASE_URI:-""}
export MERGE_BASE_URI=${MERGE_BASE_URI:-mergetb.example.net}
export XDC_BASE_URI=${XDC_BASE_URI:-mergetb.example.net}

export KUBECONFIG=./vanilla-k8s/workdir/kubeconfig

export INTERNAL_REGISTRY=false

if cat $PORTAL_CONFIG | grep -i "internal:" | grep -i "registry.internal" ; then
    export INTERNAL_REGISTRY=true
fi

# clean internal registry
ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo systemctl stop local-registry && sudo rm -rf /var/lib/registry/* && sudo systemctl start local-registry'

# install merge

pushd install
    go build

    echo Running Preflight
    if [ $INTERNAL_REGISTRY = true ]; then
        sudo ./3p-containers.sh
    fi

    mkdir -p build
    cd build/

    if [ ! -f ".conf/generated.yml" ]; then
        echo
        echo `pwd`/".conf/generated.yml" does not exist.
        echo Please copy portal-genconf-*.yml as `pwd`/".conf/generated.yml"
        exit 1
    fi

    echo Running Installer
    cp ../installer .
    cp $PORTAL_CONFIG portal-install.yml
    sed -i 's/mergetb.example.net/'"$MERGE_BASE_URI/" portal-install.yml
    sed -i 's/mergetb.example.io/'"$XDC_BASE_URI/" portal-install.yml
    ./installer -d -c portal-install.yml
popd

# install launch
./vanilla-k8s/install-launch.sh

# wait for merge install to complete
until kubectl -n merge describe job/ops-init > /dev/null
do
    echo waiting for job/ops-init to be created...
    sleep 5
done

until kubectl -n merge wait --for=condition=complete job/ops-init --timeout=10s
do
    kubectl get pods --all-namespaces=true | grep -v Running | grep -v Completed | grep -v 'ops-init.*Error'
    echo
done

# wait for things to reboot

kubectl get deployments --all-namespaces=true

table=`kubectl get deployments --all-namespaces=true | awk 'NR!=1'`
namespaces=(`echo "$table" | awk '{print $1}'`)
deploys=(`echo "$table" | awk '{print $2}'`)

for i in "${!namespaces[@]}"; do
    kubectl rollout status -n "${namespaces[i]}" deployment/"${deploys[i]}"
done

kubectl get deployments --all-namespaces=true

ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo podman image prune -a -f'

kubectl get pods -n merge | grep ops-init | awk '{print $1}' | xargs -- kubectl delete pod -n merge

kubectl get pods --all-namespaces=true

echo Portal update complete!
