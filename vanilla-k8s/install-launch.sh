#!/bin/bash

set -ex

MERGE_BASE_URI=${MERGE_BASE_URI:-mergetb.example.net}
INTERNAL_REGISTRY=${INTERNAL_REGISTRY:-true}

export REGISTRY=${MERGE_BASE_URI}:5000
export REGISTRY_PATH=merge
export DOCKER_ARGS=--no-cache
export DOCKER=podman
export DOCKER_PUSH_ARGS=--tls-verify=false
export INTERNAL=registry.internal:5000
export REMOTE=registry.gitlab.com/mergetb/portal
export TAG=v1.1.1

#$DOCKER login -u $(oc whoami) -p $(oc whoami -t) $DOCKER_PUSH_ARGS $REGISTRY

image=$REMOTE/launch:$TAG

if [ $INTERNAL_REGISTRY = true ]; then
  $DOCKER pull $REMOTE/launch:$TAG
  $DOCKER tag $REMOTE/launch:$TAG $REGISTRY/$REGISTRY_PATH/launch:$TAG
  $DOCKER push $DOCKER_PUSH_ARGS $REGISTRY/$REGISTRY_PATH/launch:$TAG

  $DOCKER push $DOCKER_PUSH_ARGS $REGISTRY/$REGISTRY_PATH/launch:$TAG

  image=$INTERNAL/$REGISTRY_PATH/launch:$TAG
fi

kubectl apply -f <(cat <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: launch
# ---
# apiVersion: v1
# metadata:
#   name: launch-tls
#   namespace: launch
# data:
#   tls.crt:
#   tls.key:
# kind: Secret
# type: kubernetes.io/tls
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: portal-launch
    app.kubernetes.io/component: portal-launch
    app.kubernetes.io/instance: portal-launch
  name: portal-launch
  namespace: launch
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      deployment: portal-launch
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        deployment: portal-launch
    spec:
      containers:
      - name: portal-launch
        image: $image
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        env:
        - name: REACT_APP_MERGE_API_URI
          value: "https://api.${MERGE_BASE_URI}"
        - name: REACT_APP_MERGE_AUTH_URI
          value: "https://auth.${MERGE_BASE_URI}"
        - name: REACT_APP_MERGE_PORTAL
          value: "${MERGE_BASE_URI}"
        - name: REACT_APP_KNOWN_TESTBEDS_URI
          value: ""
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: portal-launch
  namespace: launch
spec:
  rules:
  - host: launch.$MERGE_BASE_URI
    http:
      paths:
      - backend:
          service:
            name: portal-launch
            port:
              number: 8080
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - launch.$MERGE_BASE_URI
    secretName: launch-tls
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: portal-launch
    app.kubernetes.io/component: portal-launch
    app.kubernetes.io/instance: portal-launch
  name: portal-launch
  namespace: launch
spec:
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: 8080-tcp
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    deployment: portal-launch
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
EOF
)


kubectl -n launch rollout status deployment portal-launch
