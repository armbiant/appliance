#!/bin/bash

export KUBECONFIG=`pwd`/bootstrap-in-place-poc/sno-workdir/auth/kubeconfig
export KUBEADMIN_PW=`cat bootstrap-in-place-poc/sno-workdir/auth/kubeadmin-password`
export CORE_SSHKEY=`pwd`/bootstrap-in-place-poc/ssh-key/key
export PATH=$PATH:`pwd`
